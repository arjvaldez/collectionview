//
//  ViewController.swift
//  CollectionView
//
//  Created by Arjay on 9/23/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit



class ViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    struct Data {
         public var pics = ["Cat", "Girl", "Girl 2", "Girl", "Girl 2", "Girl", "Girl 2"]
    }
    
    let data = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(CollectionViewCell.nib(), forCellWithReuseIdentifier: "CollectionViewCell")
        
        collectionView.register(HeaderCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCollectionReusableView.identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: 90)
        layout.headerReferenceSize = CGSize(width: self.view.frame.width, height: 150)
        collectionView.collectionViewLayout = layout
        
    }

}

extension ViewController: UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        print("hello")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderCollectionReusableView.identifier, for: indexPath) as! HeaderCollectionReusableView
        
        headerView.configure()
        headerView.delegate = self
        
        return headerView

    }
        
}

extension ViewController: HeaderDelegate{
    func cellTapped(title: String?) {
        if let detailController = storyboard?.instantiateViewController(withIdentifier: "detail") as? DetailViewController{
            detailController.title = title	
        navigationController?.pushViewController(detailController, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.pics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        cell.configure(with: UIImage(named: data.pics[indexPath.row])!, text: "Text Sample", name: data.pics[indexPath.row])
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
}
