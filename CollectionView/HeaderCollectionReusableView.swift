//
//  HeaderCollectionReusableView.swift
//  CollectionView
//
//  Created by Arjay on 9/24/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

protocol HeaderDelegate {
    func cellTapped(title: String?)
}

class HeaderCollectionReusableView: UICollectionReusableView{
    
    
    @IBOutlet var headerCollectionView: UICollectionView!
    let detailVC = DetailViewController()
    let data = ViewController.Data()
    var delegate: HeaderDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        headerCollectionView.register(HeaderViewCell.nib(), forCellWithReuseIdentifier: "HeaderViewCell")
        
        headerCollectionView.dataSource = self
        headerCollectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 74, height: 95)
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        headerCollectionView.collectionViewLayout = layout

    }
    
    static let identifier = "HeaderCollectionReusableView"
    
    public func configure(){
        backgroundColor = .systemGray
    }
    
    static func nib() -> UINib{
        return UINib(nibName: "HeaderCollectionReusableView", bundle: nil)

    }
}

extension HeaderCollectionReusableView: UICollectionViewDelegate{

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    delegate?.cellTapped(title: data.pics[indexPath.row])
    }
}

extension HeaderCollectionReusableView: UICollectionViewDataSource{


func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return data.pics.count
}
    

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderViewCell", for: indexPath) as! HeaderViewCell
    if indexPath.row == 0{
        cell.configure(with: UIImage(systemName: "plus.circle")!, name: " ")
        
    }else{
        cell.configure(with: UIImage(named: data.pics[indexPath.row])!, name: data.pics[indexPath.row] )
    }
    return cell

    }
    
}


