//
//  HeaderViewCell.swift
//  CollectionView
//
//  Created by Arjay on 9/25/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class HeaderViewCell: UICollectionViewCell {
    
    @IBOutlet var personImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with image: UIImage, name: String){
         personImage.layer.cornerRadius = self.personImage.frame.size.width / 2
         personImage.image = image
         nameLabel.text = name
         
     }
   
     static func nib() -> UINib{
         return UINib(nibName: "HeaderViewCell", bundle: nil)
     }

}
