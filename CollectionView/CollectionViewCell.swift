//
//  CollectionViewCell.swift
//  CollectionView
//
//  Created by Arjay on 9/24/20.
//  Copyright © 2020 Arjay. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var personImage: UIImageView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func configure(with image: UIImage, text: String, name: String){
        personImage.layer.cornerRadius = self.personImage.frame.size.width / 2
        personImage.image = image
        textLabel.text = text
        nameLabel.text = name
        
    }
    
    public func configure(with image: UIImage){
        personImage.layer.cornerRadius = self.personImage.frame.size.width / 2
        personImage.image = image
    }
    
    static func nib() -> UINib{
        return UINib(nibName: "CollectionViewCell", bundle: nil)
    }
}
